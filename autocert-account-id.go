// Prints out the ACME account URL corresponding to the private key in an
// acme/autocert cache directory.
//
// Usage: autocert-account-id CACHEDIR
package main

import (
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"flag"
	"fmt"
	"os"

	"golang.org/x/crypto/acme"
	"golang.org/x/crypto/acme/autocert"
)

func main() {
	flag.Parse()
	if flag.NArg() != 1 {
		fmt.Fprintf(os.Stderr, "Usage: %s CACHEDIR\n", os.Args[0])
		os.Exit(1)
	}
	cache := autocert.DirCache(flag.Arg(0))

	kid, err := getKID(context.Background(), cache)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	} else {
		fmt.Println(kid)
	}
}

func getKID(ctx context.Context, cache autocert.Cache) (string, error) {
	client, err := acmeClient(ctx, cache)
	if err != nil {
		return "", err
	}
	account, err := client.GetReg(ctx, "")
	if err != nil {
		return "", fmt.Errorf("client.GetReg: %w", err)
	}
	return account.URI, nil
}

func acmeClient(ctx context.Context, cache autocert.Cache) (*acme.Client, error) {
	// https://cs.opensource.google/go/x/crypto/+/refs/tags/v0.17.0:acme/autocert/autocert.go;drc=4ba4fb4dd9e7f7ed9053fb45482e9a725c7e3fb4;l=979
	client := &acme.Client{
		DirectoryURL: autocert.DefaultACMEDirectory,
		UserAgent:    "autocert-account-id",
	}
	var err error
	client.Key, err = accountKey(ctx, cache)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func accountKey(ctx context.Context, cache autocert.Cache) (crypto.Signer, error) {
	// https://cs.opensource.google/go/x/crypto/+/refs/tags/v0.17.0:acme/autocert/autocert.go;drc=4ba4fb4dd9e7f7ed9053fb45482e9a725c7e3fb4;l=936
	const keyName = "acme_account+key"
	data, err := cache.Get(ctx, keyName)
	if err != nil {
		return nil, fmt.Errorf("cache.Get(%q): %w", keyName, err)
	}
	priv, _ := pem.Decode(data)
	return parsePrivateKey(priv.Bytes)
}

func parsePrivateKey(der []byte) (crypto.Signer, error) {
	// https://cs.opensource.google/go/x/crypto/+/refs/tags/v0.17.0:acme/autocert/autocert.go;drc=4ba4fb4dd9e7f7ed9053fb45482e9a725c7e3fb4;l=1084
	if key, err := x509.ParsePKCS1PrivateKey(der); err == nil {
		return key, nil
	}
	if key, err := x509.ParsePKCS8PrivateKey(der); err == nil {
		switch key := key.(type) {
		case *rsa.PrivateKey:
			return key, nil
		case *ecdsa.PrivateKey:
			return key, nil
		default:
			return nil, errors.New("acme/autocert: unknown private key type in PKCS#8 wrapping")
		}
	}
	if key, err := x509.ParseECPrivateKey(der); err == nil {
		return key, nil
	}
	return nil, errors.New("failed to parse private key")
}
