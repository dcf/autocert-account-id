module gitlab.torproject.org/dcf/autocert-account-id

go 1.19

require golang.org/x/crypto v0.17.0

require (
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
